package my.music.common.result;

public enum ResultEnum {
    // 成功状态码
    SUCCESS("00000","成功"),
    // -------------------失败状态码----------------------
//    USER_REGISTER_ERROR("A0100","用户注册错误"),
//    USER_SAVE_ERROR("A0100","用户注册错误"),
    USER_EXIST("A0111","账号已存在"),
    USER_NOTHINGNESS("A0201","用户账户不存在"),
    USER_PASSWORD_ERROR("A0210","密码错误"),
    USER_LOGIN_EXPIRED("A0230","用户登陆已过期"),

    UNAUTHORIZED("A0301","访问未授权"),
    NO_API_PERMISSION("A0312", "无权限使用API"),

    PARAMETER_ERROR("A0400","请求参数错误"),
    PARAMETER_NULL("A0410","请求必填参数为空"),
//    ORDER_NUMBER_NULL("A0411","用户订单号为空"),

    PARAMETER_VALUE_EXCEED("A0420", "请求参数值超出允许的范围"),

    USER_HANDLE_EXCEPTION("A0440","用户操作异常"),
    USER_REQUEST_EXCEPTION("A0500","用户请求服务异常"),
    USER_REPEAT_REQUEST("A0506","用户重复请求"),

//    SHOP_CLOSE("A1101","店铺已打烊"),
//    TRADE_SUCCESS("", "交易支付成功"),
//    TRADE_FINISHED("", "交易结束"),
    UPLOAD_FILE_EXCEPTION("A0700", "用户上传文件异常"),
    UPLOAD_FILE_TYPE_EXCEPTION("A0701", "用户上传文件类型不匹配"),
    UPLOAD_SIZE_BIG("A0702", "用户上传文件太大"),

    QR_CODE_EXPIRED("A1101", "二维码已过期"),

    SYSTEM_RUN_ERROR("B0001","系统执行出错");

    private String code;
    private String msg;

    ResultEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
