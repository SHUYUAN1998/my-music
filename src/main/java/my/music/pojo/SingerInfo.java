package my.music.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import my.music.validator.Create;
import my.music.validator.Update;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigInteger;
import java.sql.Timestamp;

/**
 * 歌手信息
 */
@Data
public class SingerInfo extends Model<SingerInfo> {
    /**
     * 歌手id
     * @ignore
     */
    @TableId(type = IdType.AUTO)
    @Null(groups = Create.class)
    @NotNull(groups = Update.class)
    private BigInteger id;

    /**
     * 歌手头像文件id
     */
    private BigInteger singerImgFileId;

    /**
     * 歌手名称
     * （最多30个字符）
     */
    @NotBlank(groups = Create.class)
    @Length(max = 30)
    private String name;

    /**
     * 添加时间
     * @ignore
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private Timestamp gmtCreate;

    /**
     * 修改时间
     * @ignore
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Timestamp gmtModified;

    /**
     * 逻辑删除
     * @ignore
     * @mock false
     */
    @TableField(value = "is_deleted", fill = FieldFill.INSERT)
    @TableLogic
    private Boolean deleted;
}
