package my.music.service;

import com.baomidou.mybatisplus.extension.service.IService;
import my.music.common.param.PageParam;
import my.music.pojo.SongInfo;

import java.math.BigInteger;

public interface SongInfoService extends IService<SongInfo> {

    boolean addSongInfo(SongInfo songInfo);
    boolean updateSongInfo(SongInfo songInfo);

    SongInfo getSongInfoById(BigInteger id);

    PageParam<SongInfo> listSongInfoPage(PageParam<SongInfo> page, SongInfo songInfo, String search);
}
