package my.music.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import my.music.common.param.PageParam;
import my.music.dao.SingerInfoMapper;
import my.music.pojo.SingerInfo;
import my.music.service.SingerInfoService;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

import static my.music.common.CacheConstants.*;

@Service
@CacheConfig(cacheNames = "singerInfo")
public class SingerInfoServiceImpl extends ServiceImpl<SingerInfoMapper, SingerInfo> implements SingerInfoService {
    @Override
    @CacheEvict(allEntries = true)
    public SingerInfo saveSingerInfo(SingerInfo singerInfo){
        singerInfo.insert();
        return singerInfo;
    }

    @Override
    @CacheEvict(allEntries = true)
    public SingerInfo updateSingerInfo(SingerInfo singerInfo) {
        singerInfo.updateById();
        return singerInfo;
    }

    @Override
    @Cacheable(key = P0, unless = UNLESS_RESULT_NULL)
    public SingerInfo getSingerInfoById(BigInteger id) {
        return this.getById(id);
    }

    @Override
    public PageParam<SingerInfo> listSingerInfoPage(PageParam<SingerInfo> pageParam, SingerInfo singerInfo, String search) {
        LambdaQueryWrapper<SingerInfo> wrapper = Wrappers.lambdaQuery(singerInfo)
                .eq(StringUtils.isNotBlank(search), SingerInfo::getId, search)
                .or()
                .like(StringUtils.isNotBlank(search), SingerInfo::getName, search);
        return singerInfo.selectPage(pageParam, wrapper);
    }


}
