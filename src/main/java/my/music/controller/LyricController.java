package my.music.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import my.music.common.result.Result;
import my.music.common.result.ResultEnum;
import my.music.common.result.ResultModel;
import my.music.dao.LyricInfoMapper;
import my.music.pojo.LyricInfo;
import my.music.validator.Create;
import my.music.validator.Update;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.groups.Default;
import java.math.BigInteger;
import java.util.List;

/**
 * 歌词
 */
@RestController
@RequestMapping("lyric")
public class LyricController {
    @Resource
    private LyricInfoMapper lyricInfoMapper;

    /**
     * 添加歌词
     * @ignoreParams id
     * @param lyricInfo
     * @return
     */
    @PostMapping("api/add")
    public Result<LyricInfo> addLyric(@Validated({Default.class, Create.class}) LyricInfo lyricInfo){
        int insert = lyricInfoMapper.insert(lyricInfo);
        if (insert > 0){
            return ResultModel.success(lyricInfo);
        }
        return ResultModel.error(ResultEnum.SYSTEM_RUN_ERROR);
    }

    /**
     * 修改歌词
     * @param lyricInfo
     * @return
     */
    @PostMapping("api/update")
    public Result updateLyric(@Validated({Default.class, Update.class}) LyricInfo lyricInfo){
        int update = lyricInfoMapper.updateById(lyricInfo);
        if (update > 0){
            return ResultModel.success();
        }
        return ResultModel.error(ResultEnum.SYSTEM_RUN_ERROR);
    }

    /**
     * 获取歌词
     * @param songId
     * @return
     */
    @GetMapping("get/{songId}")
    public Result<List<LyricInfo>> getLyricBySongId(@PathVariable("songId") BigInteger songId){
        QueryWrapper qw = new QueryWrapper<>()
                .eq("song_id", songId);
        List<LyricInfo> list = lyricInfoMapper.selectList(qw);
        return ResultModel.success(list);
    }
}
