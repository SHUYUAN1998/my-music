/*
 Navicat Premium Data Transfer

 Source Server         : MYSQL
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : my_music

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 09/01/2022 05:43:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for file_info
-- ----------------------------
DROP TABLE IF EXISTS `file_info`;
CREATE TABLE `file_info`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `md5` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '地址',
  `gmt_create` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `gmt_modified` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` tinyint(1) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for lyric_info
-- ----------------------------
DROP TABLE IF EXISTS `lyric_info`;
CREATE TABLE `lyric_info`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `song_id` bigint(20) UNSIGNED NOT NULL COMMENT '歌曲id',
  `lyric` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '歌词',
  `tlyric` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '歌词翻译',
  `gmt_create` datetime(0) NOT NULL COMMENT '添加时间',
  `gmt_modified` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` tinyint(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sheet_details
-- ----------------------------
DROP TABLE IF EXISTS `sheet_details`;
CREATE TABLE `sheet_details`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sheet_id` bigint(20) UNSIGNED NOT NULL COMMENT '歌单id',
  `song_id` bigint(20) UNSIGNED NOT NULL COMMENT '歌曲id',
  `gmt_create` datetime(0) NOT NULL COMMENT '添加时间',
  `gmt_modified` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` tinyint(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '歌单详情' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sheet_info
-- ----------------------------
DROP TABLE IF EXISTS `sheet_info`;
CREATE TABLE `sheet_info`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sheet_img_file_id` bigint(20) NULL DEFAULT NULL COMMENT '封面图片文件id',
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '用户id',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '歌单名称',
  `is_open` tinyint(1) UNSIGNED NOT NULL COMMENT '是否公开',
  `gmt_create` datetime(0) NOT NULL COMMENT '添加时间',
  `gmt_modified` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` tinyint(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '歌单信息' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for singer_info
-- ----------------------------
DROP TABLE IF EXISTS `singer_info`;
CREATE TABLE `singer_info`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `singer_img_file_id` bigint(20) NULL DEFAULT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '歌手名称',
  `gmt_create` datetime(0) NOT NULL COMMENT '添加时间',
  `gmt_modified` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` tinyint(1) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '歌手信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of singer_info
-- ----------------------------
INSERT INTO `singer_info` VALUES (1, NULL, 'Taylor Swift', '2021-12-30 19:21:30', '2021-12-30 19:21:30', 0);
INSERT INTO `singer_info` VALUES (2, NULL, 'Sia', '2022-01-03 05:49:48', '2022-01-03 05:49:48', 0);
INSERT INTO `singer_info` VALUES (3, NULL, 'Sean Paul', '2022-01-03 05:50:07', '2022-01-03 05:50:07', 0);

-- ----------------------------
-- Table structure for song_info
-- ----------------------------
DROP TABLE IF EXISTS `song_info`;
CREATE TABLE `song_info`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `song_img_file_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '封面图片文件id',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '歌曲名称',
  `song_file_id` bigint(20) UNSIGNED NOT NULL COMMENT '歌曲文件id',
  `gmt_create` datetime(0) NOT NULL COMMENT '添加时间',
  `gmt_modified` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` tinyint(1) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '歌曲信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for song_singer
-- ----------------------------
DROP TABLE IF EXISTS `song_singer`;
CREATE TABLE `song_singer`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `song_id` bigint(20) UNSIGNED NOT NULL COMMENT '歌曲id',
  `singer_id` bigint(20) UNSIGNED NOT NULL COMMENT '歌手id',
  `gmt_create` datetime(0) NOT NULL COMMENT '添加时间',
  `gmt_modified` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` tinyint(1) NOT NULL COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '歌曲歌手关联' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for song_tag
-- ----------------------------
DROP TABLE IF EXISTS `song_tag`;
CREATE TABLE `song_tag`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `song_id` bigint(20) NULL DEFAULT NULL COMMENT '歌曲id',
  `tag_id` bigint(20) NULL DEFAULT NULL COMMENT '标签id',
  `is_deleted` tinyint(1) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '歌曲标签关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tag_info
-- ----------------------------
DROP TABLE IF EXISTS `tag_info`;
CREATE TABLE `tag_info`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签名称',
  `gmt_create` datetime(0) NOT NULL COMMENT '添加时间',
  `gmt_modified` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` tinyint(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '标签信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_admin
-- ----------------------------
DROP TABLE IF EXISTS `user_admin`;
CREATE TABLE `user_admin`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `gmt_create` datetime(0) NOT NULL COMMENT '添加时间',
  `gmt_modified` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` tinyint(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_admin
-- ----------------------------
INSERT INTO `user_admin` VALUES (1, 1, '2021-12-30 21:54:45', '2021-12-30 21:54:45', 0);
INSERT INTO `user_admin` VALUES (4, 4, '2021-12-31 20:54:39', '2021-12-31 20:54:39', 0);

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `img_file_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '用户头像文件id',
  `user_name` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名称',
  `user_account` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '账号',
  `user_password` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `gmt_create` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `gmt_modified` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` tinyint(1) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES (1, NULL, 'user123', 'user123', 'b27c37b23a5142657e96ab2ef9e7a264', '2021-12-30 21:26:33', '2021-12-30 21:26:33', 0);
INSERT INTO `user_info` VALUES (4, NULL, 'xiaoming', 'xiaoming', '6711e5b38bc588ffee73ecb53d639d3a', '2021-12-31 20:50:15', '2021-12-31 20:50:15', 0);

SET FOREIGN_KEY_CHECKS = 1;
