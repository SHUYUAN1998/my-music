package my.music.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 用户密码参数
 */
@Data
public class UserPwdDTO {

    /**
     * 密码
     */
    @NotEmpty
    private String userPassword;
}
