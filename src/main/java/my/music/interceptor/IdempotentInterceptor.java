package my.music.interceptor;

import cn.dev33.satoken.stp.StpUtil;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import my.music.annotation.Idempotent;
import my.music.exception.NotRequestIdException;
import my.music.exception.RepeatabilityRequestException;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

//@Aspect
@Component
public class IdempotentInterceptor implements HandlerInterceptor {
    Cache<String, String> cache = Caffeine.newBuilder()
            .maximumSize(2000)
            .expireAfterWrite(3, TimeUnit.MINUTES)
            .build();

    private final Lock lock = new ReentrantLock();
    /**
     * 预处理回调
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 判断有没有 @Idempotent 注解
        if (handler instanceof HandlerMethod && this.existIdempotent((HandlerMethod) handler)){
            String requestId = request.getHeader("RequestId");
            // 判断有没有 requestId
            if (ObjectUtils.isEmpty(requestId)){
                throw new NotRequestIdException();
            }

            // 获取key
            String key = this.getKey(request);
            // 获取缓存
            String ifPresent = cache.getIfPresent(key);
            if(ifPresent == null){
                // 获取锁
                lock.lock();
                // 再次获取缓存
                ifPresent = cache.getIfPresent(key);
                if(ifPresent == null) {
                    // 添加缓存
                    cache.put(key, "PROC");
                    // 释放锁
                    lock.unlock();
                    return true;
                }
                // 释放锁
                lock.unlock();
            }

            throw new RepeatabilityRequestException();

        }
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    /**
     * 完全处理请求回调
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {


        // 判断有没有 @Idempotent 注解
        if (handler instanceof HandlerMethod && this.existIdempotent((HandlerMethod) handler)){
            // 获取key
            String key = this.getKey(request);
            cache.put(key, "OK");
        }

        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }

    /**
     * 判断有没有 @Idempotent 注解
     * @param handler
     * @return
     */
    private boolean existIdempotent(HandlerMethod handler){
        Idempotent idempotent = handler.getMethodAnnotation(Idempotent.class);
        if (idempotent == null){
            return false;
        }
        return true;
    }

    /**
     * 生成key
     * @param request
     * @return
     */
    private String getKey(HttpServletRequest request){
        // 请求id
        String requestId = request.getHeader("RequestId");
        // 获取请求行中的资源名称部分，即位于 URL 的主机和端口之后，参数部分之前的部分
        String requestURI = request.getRequestURI();
        // 获取客户端的 IP 地址
        String remoteAddr = request.getRemoteAddr();
        // 获取当前会话账号id, 如果未登录，则返回默认值
        Long loginId = StpUtil.getLoginId( 0L);

        return remoteAddr + requestId + loginId + requestURI;
    }
}
