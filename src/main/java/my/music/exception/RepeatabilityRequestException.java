package my.music.exception;

public class RepeatabilityRequestException extends Exception{

    public RepeatabilityRequestException() {
    }

    public RepeatabilityRequestException(String message) {
        super(message);
    }
}
