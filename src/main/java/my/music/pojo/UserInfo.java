package my.music.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import my.music.validator.Create;
import my.music.validator.Update;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigInteger;
import java.sql.Timestamp;

/*@AllArgsConstructor
@Getter
@Setter
@ToString*/
@Data
/**
 * 用户信息
 */
public class UserInfo extends Model<UserInfo> {
    /**
     * 用户id
     */
    @TableId(type = IdType.AUTO)
    @Null
    private BigInteger id;

    /**
     * 用户头像文件id
     */
    private BigInteger imgFileId;

    /**
     * 用户名称
     * （长度 4 ~ 15 个字符）
     */
    @NotBlank(groups = Create.class)
    @Length(min = 4, max = 15, message = "名称长度 4 ~ 15 个字符之间")
    private String userName;

    /**
     * 账号
     * （账号长度 6 ~ 18 个字符之间）
     * @mock user
     *
     */
    @NotBlank(groups = Create.class)
    @Length(min = 6, max = 18, message = "账号长度 6 ~ 18 个字符之间")
    @Null(groups = Update.class)
    private String userAccount;

    /**
     * 密码
     * （密码长度应当在 6 ~ 30 个字符之间）
     * @mock user123
     */
    @NotBlank(groups = Create.class)
    @Length(min = 6, max = 30, message = "密码长度应当在 6 ~ 30 个字符之间")
    @Null(groups = Update.class)

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String userPassword;

    /**
     * 添加时间
     * @ignore
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private Timestamp gmtCreate;

    /**
     * 修改时间
     * @ignore
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Timestamp gmtModified;

    /**
     * 逻辑删除
     * @ignore
     * @mock false
     */
    @TableField(value = "is_deleted", fill = FieldFill.INSERT)
    private Boolean deleted;
}
