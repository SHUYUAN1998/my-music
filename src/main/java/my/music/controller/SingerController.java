package my.music.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import my.music.common.param.PageParam;
import my.music.common.result.Result;
import my.music.common.result.ResultEnum;
import my.music.common.result.ResultModel;
import my.music.dao.SingerInfoMapper;
import my.music.pojo.SingerInfo;
import my.music.service.SingerInfoService;
import my.music.validator.Create;
import my.music.validator.Update;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.groups.Default;
import javax.websocket.server.PathParam;
import java.math.BigInteger;

/**
 * 歌手
 */
@RestController
@RequestMapping("singer")
public class SingerController {
    @Resource
    private SingerInfoService singerInfoService;

    /**
     * 添加歌手
     * @param singerInfo
     * @return
     */
    @PostMapping("api/add/info")
    public Result<SingerInfo> addSinger(@RequestBody @Validated({Default.class, Create.class}) SingerInfo singerInfo){
        singerInfoService.saveSingerInfo(singerInfo);
        return ResultModel.success(singerInfo);
    }

    /**
     * 修改歌手信息
     * @param singerInfo
     * @return
     */
    @PostMapping("api/update")
    public Result<SingerInfo> updateSinger(@RequestBody @Validated({Default.class, Update.class}) SingerInfo singerInfo){
        singerInfoService.updateSingerInfo(singerInfo);
        return ResultModel.success(singerInfo);
    }

    /**
     * 分页获取歌手列表
     * @param page
     * @param search 搜索（歌手名称）
     * @return
     */
    @GetMapping("list/page")
    public Result<PageParam<SingerInfo>> listSingerInfoPage(PageParam<SingerInfo> page,
                                                            SingerInfo singerInfo,
                                                            @Valid @Length(max = 30) String search){
        PageParam<SingerInfo> singerInfoPage = singerInfoService.listSingerInfoPage(page, singerInfo, search);
        return ResultModel.success(singerInfoPage);
    }

    /**
     * 获取歌手
     * @param id 歌手id
     * @return
     */
    @GetMapping("get/{id}")
    public Result<SingerInfo> getSingerInfo(@PathVariable("id") BigInteger id){
        SingerInfo dbSingerInfo = singerInfoService.getSingerInfoById(id);
        return ResultModel.success(dbSingerInfo);
    }

}
