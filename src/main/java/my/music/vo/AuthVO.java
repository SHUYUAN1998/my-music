package my.music.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import my.music.pojo.UserInfo;

import java.util.List;

/**
 * 登录响应
 */
@Data
@AllArgsConstructor
public class AuthVO {
    /**
     * 用户信息
     */
    private UserInfo userInfo;

    /**
     * 权限标识
     */
    private List<String> permissionList;
}
