package my.music.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import my.music.pojo.SongInfo;

import java.util.List;

/**
 * 返回音乐列表
 */
@Data
@AllArgsConstructor
public class SongInfoListVO {
    /**
     * 音乐信息
     */
    private SongInfo songInfo;

    /**
     * 歌手名称
     */
    private List<String> singerName;
}
