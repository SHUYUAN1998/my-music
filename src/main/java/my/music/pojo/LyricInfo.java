package my.music.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import my.music.validator.Create;
import my.music.validator.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigInteger;
import java.sql.Timestamp;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
/**
 * 歌词类
 */
public class LyricInfo extends Model<LyricInfo> {
    /**
     * 歌词id
     *
     */
    @TableId(type = IdType.AUTO)
    @Null(groups = Create.class)
    @NotNull(groups = Update.class)
    private BigInteger id;

    /**
     * 歌曲id
     */
    @NotNull(groups = Create.class)
    private BigInteger songId;


    /**
     * 歌词
     * @mock [00:14.190]He said the way my blue eyes shined,
     * [00:17.400]Put those Georgia stars to shame that night
     */
    @NotBlank(groups = Create.class)
    private String lyric;

    /**
     * 歌词翻译
     * @mock [00:14.190]他说我的蓝眼睛在闪烁
     * [00:17.400]让当晚所有佐治亚州的星星黯然失色
     */
    private String tlyric;

    /**
     * 添加时间
     * @ignore
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private Timestamp gmtCreate;

    /**
     * 修改时间
     * @ignore
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Timestamp gmtModified;

    /**
     * 逻辑删除
     * @ignore
     * @mock false
     */
    @TableField(value = "is_deleted", fill = FieldFill.INSERT)
    @TableLogic
    private Boolean deleted;
}
