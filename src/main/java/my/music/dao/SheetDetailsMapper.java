package my.music.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import my.music.pojo.SheetDetails;
import org.springframework.stereotype.Repository;

@Repository
public interface SheetDetailsMapper extends BaseMapper<SheetDetails> {
}
