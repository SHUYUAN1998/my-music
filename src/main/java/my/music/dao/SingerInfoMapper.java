package my.music.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import my.music.pojo.SingerInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface SingerInfoMapper extends BaseMapper<SingerInfo> {

}
