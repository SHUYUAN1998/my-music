package my.music.handler;

import cn.dev33.satoken.stp.StpInterface;
import my.music.pojo.UserAdmin;
import my.music.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * 自定义权限验证接口扩展
 */
@Component
public class StpInterfaceImpl implements StpInterface {
    @Autowired
    private UserInfoService userService;

    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        List<String> list = new ArrayList();
        List<UserAdmin> listUserAdmin = userService.listUserAdminByUserId(new BigInteger(loginId.toString()));
        if (listUserAdmin != null && listUserAdmin.size() > 0){
            // 是管理员
            list.add("system");
        }
        return list;
    }


    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        /*List<String> list = new ArrayList();
        List<UserAdmin> listUserAdmin = userService.listUserAdminByUserId(new BigInteger(loginId.toString()));
        if (listUserAdmin != null && listUserAdmin.size() > 0){
            // 是管理员
            list.add("system");
        }*/
        return null;
    }
}
