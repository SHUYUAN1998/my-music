package my.music.exception;

/**
 * 文件保存异常
 */
public class UploadFileException extends Exception{
    public UploadFileException() {
    }

    public UploadFileException(String message) {
        super(message);
    }
}
