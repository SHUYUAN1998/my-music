package my.music.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import my.music.pojo.SongTag;
import org.springframework.stereotype.Repository;

@Repository
public interface SongTagMapper extends BaseMapper<SongTag> {
}
