package my.music.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import my.music.pojo.TagInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface TagInfoMapper extends BaseMapper<TagInfo> {
}
