package my.music.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.*;

@Component
public class MyBatisPlusHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        Map map = new HashMap<String,Object>();
        map.put("deleted",false);

        map.put("gmtCreate",new Timestamp(System.currentTimeMillis()));
        map.put("gmtModified",new Timestamp(System.currentTimeMillis()));

        Iterator iterator = map.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry entry = (Map.Entry) iterator.next();
            this.setFieldValByName(String.valueOf(entry.getKey()),entry.getValue(),metaObject);
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("gmtModified",new Timestamp(System.currentTimeMillis()),metaObject);
    }
}
