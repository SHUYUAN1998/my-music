package my.music.exception;

/**
 * 文件验证异常
 */
public class UploadFileSuffixException extends Exception{
    public UploadFileSuffixException() {
    }

    public UploadFileSuffixException(String message) {
        super(message);
    }
}
