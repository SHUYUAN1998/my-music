package my.music.service;

import com.baomidou.mybatisplus.extension.service.IService;
import my.music.common.param.PageParam;
import my.music.pojo.SingerInfo;

import java.math.BigInteger;

public interface SingerInfoService extends IService<SingerInfo> {
    SingerInfo saveSingerInfo(SingerInfo singerInfo);

    SingerInfo updateSingerInfo(SingerInfo singerInfo);

    SingerInfo getSingerInfoById(BigInteger id);

    PageParam<SingerInfo> listSingerInfoPage(PageParam<SingerInfo> pageParam, SingerInfo singerInfo, String search);
}
