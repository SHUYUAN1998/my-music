package my.music.service;

import com.baomidou.mybatisplus.extension.service.IService;
import my.music.pojo.UserAdmin;
import my.music.pojo.UserInfo;

import java.math.BigInteger;
import java.util.List;

public interface UserInfoService extends IService<UserInfo> {
    UserInfo addUserInfo(UserInfo userInfo);

    UserInfo getUserInfoById(BigInteger id);

    UserInfo getUserInfoByAccount(String account);

    boolean updateUserInfoById(UserInfo userInfo);

    List<UserAdmin> listUserAdminByUserId(BigInteger userId);
}
