package my.music.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import my.music.pojo.FileInfo;
import org.springframework.stereotype.Repository;

@Repository
//public interface FileInfoMapper{
public interface FileInfoMapper extends BaseMapper<FileInfo> {
}
