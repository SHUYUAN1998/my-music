package my.music.util;

import cn.dev33.satoken.secure.SaSecureUtil;
import org.springframework.stereotype.Component;

@Component
public class PasswordUtil {

    /**
     * 密码加密
     * @param password 密码
     * @param account 账号（盐值）
     * @return
     */
    public String encryption(String password, String account){
        // md5加盐加密: md5(md5(str) + md5(salt))
        String salt = SaSecureUtil.md5BySalt(password, account);
        return salt;
    }
}
