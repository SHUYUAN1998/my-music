package my.music.handler;

import cn.dev33.satoken.exception.NotLoginException;

import cn.dev33.satoken.exception.NotRoleException;
import cn.dev33.satoken.exception.NotSafeException;
import my.music.common.result.Result;
import my.music.common.result.ResultEnum;
import my.music.common.result.ResultModel;
import my.music.exception.NotRequestIdException;
import my.music.exception.RepeatabilityRequestException;
import my.music.exception.UploadFileException;
import my.music.exception.UploadFileSuffixException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

/**
 * 全局异常处理器
 */
@RestControllerAdvice
public class MyExceptionHandler {

    /**
     * token无效
     * @return
     */
    @ExceptionHandler(value = NotLoginException.class)
    public Result tokenExceptionHandler(){
        return ResultModel.error(ResultEnum.USER_LOGIN_EXPIRED);
    }

    /**
     * 二级认证失败
     * @return
     */
    @ExceptionHandler(value = NotSafeException.class)
    public Result notSafeException(){
        return ResultModel.error(ResultEnum.NO_API_PERMISSION);
    }

    /**
     * 无此角色
     * 不是管理员
     * @return
     */
    @ExceptionHandler(value = NotRoleException.class)
    public Result notRoleException(){
        return ResultModel.error(ResultEnum.UNAUTHORIZED);
    }

    /**
     * 请求参数值超出允许的范围
     * @return
     */
    @ExceptionHandler(value = BindException.class)
    public Result validationBindException(){
        return ResultModel.error(ResultEnum.PARAMETER_VALUE_EXCEED);
    }

    /**
     * 用户上传文件太大
     * @return
     */
    @ExceptionHandler(value = MaxUploadSizeExceededException.class)
    public Result multipartMaxUploadSizeExceededException(){
        return ResultModel.error(ResultEnum.UPLOAD_SIZE_BIG);
    }

    /**
     * 上传文件保存异常
     * @return
     */
    @ExceptionHandler(value = UploadFileException.class)
    public Result uploadFileException(){
        return ResultModel.error(ResultEnum.UPLOAD_FILE_EXCEPTION);
    }

    /**
     * 用户上传文件类型不匹配
     * @return
     */
    @ExceptionHandler(value = UploadFileSuffixException.class)
    public Result uploadFileSuffixException(){
        return ResultModel.error(ResultEnum.UPLOAD_FILE_TYPE_EXCEPTION);
    }

    /**
     * 请求参数错误
     * @return
     */
    @ExceptionHandler({HttpRequestMethodNotSupportedException.class, NotRequestIdException.class})
    public Result httpRequestMethodNotSupportedException(){
        return ResultModel.error(ResultEnum.PARAMETER_ERROR);
    }

    /**
     * 参数不匹配
     * @return
     */
    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    public Result methodArgumentTypeMismatchException(){
        return ResultModel.error(ResultEnum.PARAMETER_ERROR);
    }

    /**
     * 重复请求
     * @return
     */
    @ExceptionHandler(RepeatabilityRequestException.class)
    public Result repeatabilityRequestException(){
        return ResultModel.error(ResultEnum.USER_REPEAT_REQUEST);
    }
}
