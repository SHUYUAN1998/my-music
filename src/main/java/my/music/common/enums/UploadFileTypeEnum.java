package my.music.common.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum UploadFileTypeEnum {
    SONG("/song", Arrays.asList(".mp3", ".wav")),
    IMAGE("/image", Arrays.asList(".jpg", ".jpeg", ".png"));

    /**
     * 路径
     */
    private String path;

    /**
     * 后缀
     */
    private List<String> suffix;

    UploadFileTypeEnum(String path, List<String> suffix) {
        this.path = path;
        this.suffix = suffix;
    }

    public String getPath() {
        return path;
    }

    public List<String> getSuffix() {
        return suffix;
    }
}
