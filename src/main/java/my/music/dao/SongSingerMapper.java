package my.music.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import my.music.pojo.SongSinger;
import org.springframework.stereotype.Repository;

@Repository
public interface SongSingerMapper extends BaseMapper<SongSinger> {
}
