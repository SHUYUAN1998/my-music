package my.music.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import my.music.pojo.SheetInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface SheetInfoMapper extends BaseMapper<SheetInfo> {
}
