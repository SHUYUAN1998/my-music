package my.music.controller;

import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import my.music.common.param.PageParam;
import my.music.common.result.Result;
import my.music.common.result.ResultEnum;
import my.music.common.result.ResultModel;
import my.music.dao.*;
import my.music.pojo.SheetDetails;
import my.music.pojo.SheetInfo;
import my.music.pojo.SongInfo;
import my.music.pojo.SongSinger;
import my.music.service.SongInfoService;
import my.music.validator.Create;
import my.music.validator.Update;
import my.music.vo.SheetSongVO;
import my.music.vo.SongInfoListVO;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import javax.websocket.server.PathParam;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * 歌曲
 */
@RestController
@RequestMapping("song")
public class SongController {
    @Resource
    private SongInfoService songInfoService;

    @Resource
    private SongSingerMapper songSingerMapper;
    @Resource
    private SingerInfoMapper singerInfoMapper;
    @Resource
    private SheetInfoMapper sheetInfoMapper;
    @Resource
    private SheetDetailsMapper sheetDetailsMapper;

    /**
     * 添加歌曲
     * @param songInfo
     * @param songSingers 歌手id|[1,2]
     * @return
     */
    @PostMapping("api/add")
    @Transactional
    public Result<SongInfo> addSongInfo(@Validated({Default.class, Create.class}) SongInfo songInfo, @Valid @NotNull BigInteger[] songSingers){
        /*boolean res = true;
        int insert = songInfoMapper.insert(songInfo);
        if (insert <= 0){
            res = false;
        }*/
        songInfoService.addSongInfo(songInfo);

        /*SongSinger songSinger = new SongSinger();
        songSinger.setSongId(songInfo.getId());

        for (BigInteger singerId : songSingers){
            // 清除回写的id
            songSinger.setId(null);
            // 设置歌手id
            songSinger.setSingerId(singerId);

            int insertSinger = songSingerMapper.insert(songSinger);
            if (insertSinger <= 0){
                res = false;
            }
        }

        if (res){
            return ResultModel.success(songSinger);
        }
        return ResultModel.error(ResultEnum.SYSTEM_RUN_ERROR);*/

        return ResultModel.success(songInfo);
    }

    /**
     * 修改歌曲信息
     * @param songInfo
     * @return
     */
    @PostMapping("api/update")
    public Result updateSongInfo(@Validated({Default.class, Update.class}) SongInfo songInfo){
        songInfoService.updateSongInfo(songInfo);
        return ResultModel.success(songInfo);
    }

    /**
     * 歌曲追加歌手
     * @param songSinger
     * @return
     */
    @PostMapping("api/add/singer")
    public Result<SongSinger> addSongSinger(@Validated SongSinger songSinger){
        int insert = songSingerMapper.insert(songSinger);
        if (insert > 0){
            return ResultModel.success(songSinger);
        }
        return ResultModel.error(ResultEnum.SYSTEM_RUN_ERROR);
    }

    /**
     * 删除歌曲里的歌手
     * @param id 歌曲歌手关联id|1
     * @return
     */
    @GetMapping("api/delete/singer/{id}")
    public Result deleteSongSinger(@PathVariable("id") BigInteger id){
        int i = songSingerMapper.deleteById(id);
        if (i > 0){
            return ResultModel.success();
        }
        return ResultModel.error(ResultEnum.SYSTEM_RUN_ERROR);
    }


    /**
     * 分页获取歌曲列表
     * @param page
     * @param songInfo
     * @param search id 歌曲名称
     * @return
     */
    @GetMapping("list/page")
    public Result<PageParam<SongInfo>> listSongInfoPage(PageParam<SongInfo> page,
                                                         SongInfo songInfo,
                                                         String search){
        /*Page<SongInfo> page = new Page();
        page.setCurrent(current);
        if (size != null) page.setSize(size);

        if (StringUtils.isNotBlank(search)){
            IPage<SongInfo> songInfoIPage = songInfoMapper.searchSongInfo(page, search);
            List<SongInfoListVO> resList = listSingerName(songInfoIPage.getRecords());
        }
        page = songInfoMapper.selectPage(page, null);

        List<SongInfoListVO> resList = listSingerName(page.getRecords());*/

        PageParam<SongInfo> songInfoPageParam = songInfoService.listSongInfoPage(page, songInfo, search);
        return ResultModel.success(songInfoPageParam);
    }


    /**
     * 获取歌单内歌曲列表
     * @param current 页码
     * @param size 长度
     * @param id 歌单信息id
     * @return
     */
    /*@GetMapping("listBySheet/{id}/{current}")
    public Result<List<SheetSongVO>> listSongInfoBySheetId(@PathVariable("current") Integer current,
                                                           @PathParam("size") Integer size,
                                                           @PathVariable("id") BigInteger id){
        SheetInfo sheetInfo = sheetInfoMapper.selectById(id);
        if (sheetInfo == null){
            // 歌单不存在
            return ResultModel.success();
        }
        if (!sheetInfo.getOpen()){
            // 歌单不公开

            // 获取登录id
            Object loginId = StpUtil.getLoginId();
            if (!loginId.toString().equals(sheetInfo.getUserId().toString())){
                // 不是自己的歌单
                return ResultModel.error(ResultEnum.UNAUTHORIZED);
            }
        }
        QueryWrapper sheetQW = new QueryWrapper<>()
                .select("id","song_id");
        Page<SheetDetails> page = new Page();
        page.setCurrent(current);
        if (size != null) page.setSize(size);

        page = sheetDetailsMapper.selectPage(page, sheetQW);

        if (page.getRecords().size() <= 0){
            // 歌单内没有音乐
            return ResultModel.success();
        }

        List resList = new ArrayList<>();
        page.getRecords().forEach(sd -> {
            // 歌单详情信息
            SheetSongVO sheetSongVO = new SheetSongVO();
            sheetSongVO.setSheetDetails(sd);
            // 获取歌曲信息
            SongInfo songInfo = songInfoMapper.selectById(sd.getSongId());
            sheetSongVO.setSongInfo(songInfo);
            // 根据id获取歌手名称
            String[] arrSingerName = singerInfoMapper.listSingerNameBySongId(songInfo.getId());
            sheetSongVO.setSingerName(arrSingerName);

            resList.add(sheetSongVO);
        });

        return ResultModel.success(resList);
    }*/
}
