package my.music.common.enums;

/**
 * 扫码登录状态枚举
 */
public enum LoginQRCodeEnum {
    /**
     * 过期
     */
    EXPIRE,

    /**
     * 未使用
     */
    UNUSED,

    /**
     * 已扫码
     */
    CONFIRMING,

    /**
     * 已确认
     */
    CONFIRMED,

    /**
     * 取消
     */
    CANCEL;

}
