package my.music.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import my.music.pojo.LyricInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface LyricInfoMapper extends BaseMapper<LyricInfo> {
}
