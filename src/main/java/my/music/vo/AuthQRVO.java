package my.music.vo;

import lombok.Data;
import my.music.common.enums.LoginQRCodeEnum;
import my.music.pojo.UserInfo;

import java.util.List;

@Data
public class AuthQRVO{

    private LoginQRCodeEnum loginQRCodeEnum;

    /**
     * 用户信息
     */
    private UserInfo userInfo;

    /**
     * 权限标识
     */
    private List<String> permissionList;

    /**
     * 用户token
     */
    private String authorization;
}
