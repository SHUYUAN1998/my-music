package my.music.common.result;

import java.io.Serializable;

public class Result<T> implements Serializable {
    /**
     * 响应码
     * @mock 00000
     */
    private String code;

    /**
     * 描述
     * @mock 请求成功
     */
    private String msg;

    /**
     * 数据
     */
    private T data ;


    public Result() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Result{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
