package my.music.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import my.music.pojo.UserAdmin;
import my.music.pojo.UserInfo;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface UserAdminMapper extends BaseMapper<UserAdmin> {
    /*@Select("""
            SELECT u.id, u.img_file_id imgFileId, u.user_name userName, u.user_account userAccount, u.gmt_create gmtCreate, u.gmt_modified gmtModified
            FROM user_info u, user_admin ua
            WHERE u.id = ua.user_id
            """)
    IPage<UserInfo> listAdminUserInfoPage(Page page);*/

    @Select("""
            SELECT
            ua.id, ua.user_id userId, ua.gmt_create gmtCreate, ua.gmt_modified gmtModified, ua.is_deleted deleted,
            ui.id 'userInfo.id', ui.img_file_id 'userInfo.imgFileId', ui.user_name 'userInfo.userName',
            ui.user_account 'userInfo.userAccount', ui.gmt_create 'userInfo.gmtCreate',
            ui.gmt_modified 'userInfo.gmtModified', ua.is_deleted 'userInfo.deleted' 
            FROM user_admin ua, user_info ui 
            WHERE ua.user_id = ui.id
            """)
    IPage<UserAdmin> listAdminUserInfoPage(Page page);
}
