package my.music;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

// 扫描 mapper 文件夹
@MapperScan("my.music.dao")
@SpringBootApplication
@EnableCaching
public class MyMusicApplication {
	public static void main(String[] args) {
		SpringApplication.run(MyMusicApplication.class, args);
	}

}
