package my.music.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import my.music.validator.Create;
import my.music.validator.Update;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigInteger;
import java.sql.Timestamp;

/**
 * 歌单信息
 */
@Data
public class SheetInfo {
    /**
     * 歌单信息id
     */
    @TableId(type = IdType.AUTO)
    @Null(groups = Create.class)
    @NotNull(groups = Update.class)
    private BigInteger id;

    /**
     * 用户id
     * @ignore
     */
    private BigInteger userId;

    /**
     * 歌单封面图片文件id
     */
    private BigInteger sheetImgFileId;

    /**
     * 歌单名称
     * （30 个字符之内）
     * @mock 轻松的音乐
     */
    @NotBlank(groups = Create.class)
    @Length(max = 30, message = "30 个字符之内")
    private String name;

    /**
     * 是否公开歌单
     */
    @NotNull(groups = Create.class)
    @TableField(value = "is_open")
    private Boolean open;

    /**
     * 添加时间
     * @ignore
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private Timestamp gmtCreate;

    /**
     * 修改时间
     * @ignore
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Timestamp gmtModified;

    /**
     * 逻辑删除
     * @ignore
     * @mock false
     */
    @TableField(value = "is_deleted", fill = FieldFill.INSERT)
    @TableLogic
    private Boolean deleted;
}
