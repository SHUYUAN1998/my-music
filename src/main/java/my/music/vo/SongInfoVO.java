package my.music.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import my.music.pojo.SongInfo;

/**
 * 返回音乐详情
 */
@Data
@AllArgsConstructor
public class SongInfoVO {
    /**
     * 音乐信息
     */
    private SongInfo songInfo;

    /**
     * 歌手名称
     */
    private String[] singerName;
}
