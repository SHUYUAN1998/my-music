package my.music.controller;

import cn.dev33.satoken.annotation.SaCheckRole;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import my.music.common.result.Result;
import my.music.common.result.ResultEnum;
import my.music.common.result.ResultModel;
import my.music.dao.SongTagMapper;
import my.music.dao.TagInfoMapper;
import my.music.pojo.SongTag;
import my.music.pojo.TagInfo;
import my.music.validator.Create;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.groups.Default;
import javax.websocket.server.PathParam;
import java.math.BigInteger;
import java.util.List;

/**
 * 歌曲标签
 */
@RestController
@RequestMapping("tag")
public class TagController {
    @Resource
    private TagInfoMapper tagInfoMapper;
    @Resource
    private SongTagMapper songTagMapper;

    /**
     * 添加标签信息
     * @return
     */
    @PostMapping("api/add")
    @SaCheckRole("admin")
    public Result<TagInfo> addTagInfo(@Validated({Default.class, Create.class}) TagInfo tagInfo){
        int insert = tagInfoMapper.insert(tagInfo);
        if (insert > 0){
            return ResultModel.success(tagInfo);
        }
        return ResultModel.error(ResultEnum.SYSTEM_RUN_ERROR);
    }

    /**
     * 获取标签列表
     * @param current 页码
     * @param size 长度
     * @param search 搜索内容(标签名)
     * @return
     */
    @GetMapping("api/list/{current}/{size}")
    @SaCheckRole("admin")
    public Result<Page<TagInfo>> listTagInfo(@PathVariable("current") Integer current,
                                    @PathVariable("size") Integer size,
                                    @PathParam(value = "search") String search){
        QueryWrapper qw = new QueryWrapper<>()
            .like(StringUtils.isNotBlank(search), "name", search);
        Page page = new Page(current, size);
        page = tagInfoMapper.selectPage(page, qw);
        return ResultModel.success(page);
    }


    /**
     * 添加歌曲标签
     * @param songTag
     * @return
     */
    @PostMapping("api/add/song/tag")
    @SaCheckRole("admin")
    public Result<SongTag> addSongTag(@Validated({Default.class, Create.class}) SongTag songTag){
        int insert = songTagMapper.insert(songTag);
        if (insert > 0){
            return ResultModel.success(songTag);
        }
        return ResultModel.error(ResultEnum.SYSTEM_RUN_ERROR);
    }

    /**
     * 获取歌曲标签
     * @param songId 歌曲id
     * @return
     */
    @GetMapping("list/song/tag/{songId}")
    public Result<List<SongTag>> listSongTag(@PathVariable("songId") BigInteger songId){
        QueryWrapper qw = new QueryWrapper<>()
                .eq("song_id", songId);
        List<SongTag> list = songTagMapper.selectList(qw);
        return ResultModel.success(list);
    }
}
