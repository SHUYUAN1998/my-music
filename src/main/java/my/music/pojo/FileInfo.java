package my.music.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigInteger;
import java.sql.Timestamp;

@Data
public class FileInfo extends Model<FileInfo> {
    /**
     * 文件id
     */
    @TableId(type = IdType.AUTO)
    private BigInteger id;

    /**
     * 文件md5
     */
    private String md5;

    /**
     * 文件url地址
     * @mock /upload/song/1475832226223280128.mp3
     */
    private String url;

    /**
     * 添加时间
     * @ignore
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private Timestamp gmtCreate;

    /**
     * 修改时间
     * @ignore
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Timestamp gmtModified;

    /**
     * 逻辑删除
     * @mock false
     */
    @TableField(value = "is_deleted", fill = FieldFill.INSERT)
    private Boolean deleted;
}
