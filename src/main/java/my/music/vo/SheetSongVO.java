package my.music.vo;

import lombok.Data;
import my.music.pojo.SheetDetails;
import my.music.pojo.SongInfo;

/**
 * 歌单歌曲
 */
@Data
public class SheetSongVO {
    /**
     * 歌单详情信息
     */
    private SheetDetails sheetDetails;

    /**
     * 音乐信息
     */
    private SongInfo songInfo;

    /**
     * 歌手名称
     */
    private String[] singerName;
}
