package my.music.common;

/**
 * 缓存常量
 */
public class CacheConstants {
    public static final String P0 = "#p0";
    public static final String USER_INFO_ID_KEY = "#userInfo.id";
    public static final String USER_INFO_KEY = "#userInfo";

    public static final String UNLESS_RESULT_NULL= "#result == null";

//    public static final String PAGE_PARAM_CURRENT= "#pageParam.current";
//    public static final String PAGE_PARAM_CURRENT_SEARCH= "#pageParam.current + #search";

    public static final String SINGER_INFO_ID_KEY = "#singerInfo.id";

}
