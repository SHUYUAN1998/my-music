package my.music.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import my.music.common.param.PageParam;
import my.music.dao.SongInfoMapper;
import my.music.pojo.SongInfo;
import my.music.service.SongInfoService;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public class SongInfoServiceImpl extends ServiceImpl<SongInfoMapper, SongInfo> implements SongInfoService {


    @Override
    public boolean addSongInfo(SongInfo songInfo) {
        songInfo.insert(); // 插入歌手信息


        return true;
    }

    @Override
    public boolean updateSongInfo(SongInfo songInfo) {
        return songInfo.updateById();
    }

    @Override
    public SongInfo getSongInfoById(BigInteger id) {
        return this.getById(id);
    }

    @Override
    public PageParam<SongInfo> listSongInfoPage(PageParam<SongInfo> page, SongInfo songInfo, String search) {
        LambdaQueryWrapper<SongInfo> wrapper = Wrappers.lambdaQuery(songInfo)
                .eq(StringUtils.isNotBlank(search), SongInfo::getId, search)
                .or()
                .eq(StringUtils.isNotBlank(search), SongInfo::getName, search);
        return songInfo.selectPage(page, wrapper);
    }
}
