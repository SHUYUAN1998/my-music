package my.music.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import my.music.pojo.SongInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public interface SongInfoMapper extends BaseMapper<SongInfo> {

    /*@Select("""
            SELECT id 'songInfo.id', song_img_file_id 'songInfo.songImgFileId', name 'songInfo.name', song_file_id 'songInfo.songFileId', gmt_create 'songInfo.gmtCreate', gmt_modified 'songInfo.gmtModified', is_deleted 'songInfo.deleted'
            FROM song_info
            WHERE name like '%#{search}%'
            OR id IN (
            	SELECT song_id FROM song_singer
            	WHERE singer_id IN (
            		SELECT id FROM singer_info WHERE name like '%#{search}%'
            	)
            )
            """)*/
    @Select("""
            SELECT id, song_img_file_id, name, song_file_id, gmt_create, gmt_modified, is_deleted
            FROM song_info
            WHERE name like "%"#{search}"%"
            OR id IN (
            	SELECT song_id FROM song_singer
            	WHERE singer_id IN (
            		SELECT id FROM singer_info WHERE name like "%"#{search}"%"
            	)
            )
            """)
    IPage<SongInfo> searchSongInfo(Page<SongInfo> page, @Param("search") String search);
}
