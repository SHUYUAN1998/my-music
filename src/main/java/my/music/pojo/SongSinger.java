package my.music.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import my.music.validator.Create;
import my.music.validator.Update;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigInteger;
import java.sql.Timestamp;

/**
 * 歌曲歌手关联
 */
@Data
public class SongSinger {
    /**
     * 歌曲歌手关联id
     * @ignore
     */
    @TableId(type = IdType.AUTO)
    @Null(groups = Create.class)
    @NotNull(groups = Update.class)
    private BigInteger id;

    /**
     * 歌曲id
     */
    @NotNull(groups = Create.class)
    private BigInteger songId;

    /**
     * 歌手id
     */
    @NotNull(groups = Create.class)
    private BigInteger singerId;

    /**
     * 添加时间
     * @ignore
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private Timestamp gmtCreate;

    /**
     * 修改时间
     * @ignore
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Timestamp gmtModified;

    /**
     * 逻辑删除
     * @ignore
     * @mock false
     */
    @TableField(value = "is_deleted", fill = FieldFill.INSERT)
    @TableLogic
    private Boolean deleted;
}
