package my.music.exception;

public class NotRequestIdException extends Exception{
    public NotRequestIdException() {
    }

    public NotRequestIdException(String message) {
        super(message);
    }
}
