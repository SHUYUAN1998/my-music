package my.music.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import my.music.dao.UserAdminMapper;
import my.music.dao.UserInfoMapper;
import my.music.pojo.UserAdmin;
import my.music.pojo.UserInfo;
import my.music.service.UserInfoService;
import my.music.util.PasswordUtil;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.List;

import static my.music.common.CacheConstants.*;

@Service
@CacheConfig(cacheNames = "userInfo")
public class UserServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {
    @Resource
    private UserAdminMapper userAdminMapper;
    @Resource
    private PasswordUtil passwordUtil;

    @Override
    @CachePut(key = USER_INFO_ID_KEY, unless="#result == null")
    public UserInfo addUserInfo(UserInfo userInfo) {
        userInfo.setUserPassword(passwordUtil.encryption(userInfo.getUserPassword(), userInfo.getUserAccount()));
//        userInfoMapper.insert(userInfo);
        this.save(userInfo); // 向数据库插入记录
        return userInfo;
    }

    @Override
    @Cacheable(key = P0, unless="#result == null")
    public UserInfo getUserInfoById(BigInteger id){
//        return userInfoMapper.selectById(id);
        return this.getById(id);
    }

    @Override
    public UserInfo getUserInfoByAccount(String account) {
        /*QueryWrapper qw = new QueryWrapper<>()
                .eq("user_account", account);
        return userInfoMapper.selectOne(qw);*/

        LambdaQueryWrapper<UserInfo> wrapper = Wrappers.lambdaQuery(UserInfo.class)
                .eq(UserInfo::getUserAccount, account);
        return this.getOne(wrapper);
    }


    @Override
    @CacheEvict(key = USER_INFO_ID_KEY)
    public boolean updateUserInfoById(UserInfo userInfo){
        // 判断账号密码是否为空
        if (!ObjectUtils.isEmpty(userInfo.getUserAccount()) && !ObjectUtils.isEmpty(userInfo.getUserPassword())){
            userInfo.setUserPassword(passwordUtil.encryption(userInfo.getUserPassword(), userInfo.getUserAccount()));
        }
//       return userInfoMapper.updateById(userInfo);

        return userInfo.updateById();
    }

    @Override
    @Cacheable(cacheNames = "userAdminByUserId", key = P0, unless="#result == null")
    public List<UserAdmin> listUserAdminByUserId(BigInteger userId) {
        QueryWrapper adminQw = new QueryWrapper<>()
                .eq("user_id", userId);
        List<UserAdmin> listRole = userAdminMapper.selectList(adminQw);
        return listRole;
    }
}
