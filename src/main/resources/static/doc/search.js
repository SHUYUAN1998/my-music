let api = [];
const apiDocListSize = 1
api.push({
    name: 'default',
    order: '1',
    list: []
})
api[0].list.push({
    alias: 'AdminController',
    order: '1',
    link: '管理员',
    desc: '管理员',
    list: []
})
api[0].list[0].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/admin/api/add',
    desc: '添加管理员',
});
api[0].list[0].list.push({
    order: '2',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/admin/api/delete/{id}',
    desc: '删除管理员权限',
});
api[0].list[0].list.push({
    order: '3',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/admin/api/list/admin/{current}',
    desc: '分页获取管理员信息',
});
api[0].list.push({
    alias: 'AuthController',
    order: '2',
    link: '认证',
    desc: '认证',
    list: []
})
api[0].list[1].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/auth/login/account',
    desc: '获取认证（登录成功，token位于响应头中，请求时token格式为：”Authorization：\'Bearer \' + token“）',
});
api[0].list[1].list.push({
    order: '2',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/auth/api/open/safe',
    desc: '二级认证（成功后有120秒）',
});
api[0].list[1].list.push({
    order: '3',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/auth/api/logout',
    desc: '注销',
});
api[0].list[1].list.push({
    order: '4',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/auth/api/list/permission',
    desc: '获取权限标识',
});
api[0].list[1].list.push({
    order: '5',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/auth/get/qr/code/token',
    desc: '获取登录操作token',
});
api[0].list[1].list.push({
    order: '6',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/auth/get/qr/code/{token}',
    desc: '获取扫码响应',
});
api[0].list[1].list.push({
    order: '7',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/auth/api/update/qr/code/{token}/{status}',
    desc: '确认登录',
});
api[0].list.push({
    alias: 'FileController',
    order: '3',
    link: '文件',
    desc: '文件',
    list: []
})
api[0].list[2].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/file/api/song',
    desc: '上传歌曲',
});
api[0].list[2].list.push({
    order: '2',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/file/api/img',
    desc: '上传图片',
});
api[0].list[2].list.push({
    order: '3',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/file/get/{id}',
    desc: '根据文件id获取（只能获取标记未删除的文件）',
});
api[0].list[2].list.push({
    order: '4',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/file/api/get/{id}',
    desc: '根据文件id获取',
});
api[0].list[2].list.push({
    order: '5',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/file/api/remove/{id}',
    desc: '标记删除文件',
});
api[0].list.push({
    alias: 'LyricController',
    order: '4',
    link: '歌词',
    desc: '歌词',
    list: []
})
api[0].list[3].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/lyric/api/add',
    desc: '添加歌词',
});
api[0].list[3].list.push({
    order: '2',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/lyric/api/update',
    desc: '修改歌词',
});
api[0].list[3].list.push({
    order: '3',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/lyric/get/{songId}',
    desc: '获取歌词',
});
api[0].list.push({
    alias: 'SheetController',
    order: '5',
    link: '歌单',
    desc: '歌单',
    list: []
})
api[0].list[4].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/sheet/api/add/info',
    desc: '创建歌单信息',
});
api[0].list[4].list.push({
    order: '2',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/sheet/api/list/{current}',
    desc: '分页获取我的歌单',
});
api[0].list[4].list.push({
    order: '3',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/sheet/list/{current}',
    desc: '分页获取公开的歌单',
});
api[0].list[4].list.push({
    order: '4',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/sheet/api/update',
    desc: '修改我的歌单信息',
});
api[0].list[4].list.push({
    order: '5',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/sheet/api/add/details',
    desc: '添加歌曲到歌单',
});
api[0].list[4].list.push({
    order: '6',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/sheet/api/delete/details/{id}',
    desc: '删除歌单内的歌曲',
});
api[0].list.push({
    alias: 'SingerController',
    order: '6',
    link: '歌手',
    desc: '歌手',
    list: []
})
api[0].list[5].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/singer/api/add/info',
    desc: '添加歌手',
});
api[0].list[5].list.push({
    order: '2',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/singer/api/update',
    desc: '修改歌手信息',
});
api[0].list[5].list.push({
    order: '3',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/singer/list/page',
    desc: '分页获取歌手列表',
});
api[0].list[5].list.push({
    order: '4',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/singer/get/{id}',
    desc: '获取歌手',
});
api[0].list.push({
    alias: 'SongController',
    order: '7',
    link: '歌曲',
    desc: '歌曲',
    list: []
})
api[0].list[6].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/song/api/add',
    desc: '添加歌曲',
});
api[0].list[6].list.push({
    order: '2',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/song/api/update',
    desc: '修改歌曲信息',
});
api[0].list[6].list.push({
    order: '3',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/song/api/add/singer',
    desc: '歌曲追加歌手',
});
api[0].list[6].list.push({
    order: '4',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/song/api/delete/singer/{id}',
    desc: '删除歌曲里的歌手',
});
api[0].list[6].list.push({
    order: '5',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/song/list/{current}',
    desc: '分页获取歌曲列表',
});
api[0].list[6].list.push({
    order: '6',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/song/listBySheet/{id}/{current}',
    desc: '获取歌单内歌曲列表',
});
api[0].list.push({
    alias: 'TagController',
    order: '8',
    link: '歌曲标签',
    desc: '歌曲标签',
    list: []
})
api[0].list[7].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/tag/api/add',
    desc: '添加标签信息',
});
api[0].list[7].list.push({
    order: '2',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/tag/api/list/{current}/{size}',
    desc: '获取标签列表',
});
api[0].list[7].list.push({
    order: '3',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/tag/api/add/song/tag',
    desc: '添加歌曲标签',
});
api[0].list[7].list.push({
    order: '4',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/tag/list/song/tag/{songId}',
    desc: '获取歌曲标签',
});
api[0].list.push({
    alias: 'UserController',
    order: '9',
    link: '用户',
    desc: '用户',
    list: []
})
api[0].list[8].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/user/add',
    desc: '用户注册',
});
api[0].list[8].list.push({
    order: '2',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/user/api/get',
    desc: '获取用户',
});
api[0].list[8].list.push({
    order: '3',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/user/api/update',
    desc: '修改用户信息',
});
api[0].list[8].list.push({
    order: '4',
    deprecated: 'false',
    url: 'http://127.0.0.1:8092/user/api/safe/update/password',
    desc: '修改密码（需要在二级认证时间内）',
});
document.onkeydown = keyDownSearch;
function keyDownSearch(e) {
    const theEvent = e;
    const code = theEvent.keyCode || theEvent.which || theEvent.charCode;
    if (code == 13) {
        const search = document.getElementById('search');
        const searchValue = search.value.toLocaleLowerCase();

        let searchGroup = [];
        for (let i = 0; i < api.length; i++) {

            let apiGroup = api[i];

            let searchArr = [];
            for (let i = 0; i < apiGroup.list.length; i++) {
                let apiData = apiGroup.list[i];
                const desc = apiData.desc;
                if (desc.toLocaleLowerCase().indexOf(searchValue) > -1) {
                    searchArr.push({
                        order: apiData.order,
                        desc: apiData.desc,
                        link: apiData.link,
                        list: apiData.list
                    });
                } else {
                    let methodList = apiData.list || [];
                    let methodListTemp = [];
                    for (let j = 0; j < methodList.length; j++) {
                        const methodData = methodList[j];
                        const methodDesc = methodData.desc;
                        if (methodDesc.toLocaleLowerCase().indexOf(searchValue) > -1) {
                            methodListTemp.push(methodData);
                            break;
                        }
                    }
                    if (methodListTemp.length > 0) {
                        const data = {
                            order: apiData.order,
                            desc: apiData.desc,
                            link: apiData.link,
                            list: methodListTemp
                        };
                        searchArr.push(data);
                    }
                }
            }
            if (apiGroup.name.toLocaleLowerCase().indexOf(searchValue) > -1) {
                searchGroup.push({
                    name: apiGroup.name,
                    order: apiGroup.order,
                    list: searchArr
                });
                continue;
            }
            if (searchArr.length === 0) {
                continue;
            }
            searchGroup.push({
                name: apiGroup.name,
                order: apiGroup.order,
                list: searchArr
            });
        }
        let html;
        if (searchValue == '') {
            const liClass = "";
            const display = "display: none";
            html = buildAccordion(api,liClass,display);
            document.getElementById('accordion').innerHTML = html;
        } else {
            const liClass = "open";
            const display = "display: block";
            html = buildAccordion(searchGroup,liClass,display);
            document.getElementById('accordion').innerHTML = html;
        }
        const Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;
            const links = this.el.find('.dd');
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown);
        };
        Accordion.prototype.dropdown = function (e) {
            const $el = e.data.el;
            $this = $(this), $next = $this.next();
            $next.slideToggle();
            $this.parent().toggleClass('open');
            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp("20").parent().removeClass('open');
            }
        };
        new Accordion($('#accordion'), false);
    }
}

function buildAccordion(apiGroups, liClass, display) {
    let html = "";
    let doc;
    if (apiGroups.length > 0) {
         if (apiDocListSize == 1) {
            let apiData = apiGroups[0].list;
            for (let j = 0; j < apiData.length; j++) {
                html += '<li class="'+liClass+'">';
                html += '<a class="dd" href="#_' + apiData[j].link + '">' + apiData[j].order + '.&nbsp;' + apiData[j].desc + '</a>';
                html += '<ul class="sectlevel2" style="'+display+'">';
                doc = apiData[j].list;
                for (let m = 0; m < doc.length; m++) {
                    let spanString;
                    if (doc[m].deprecated == 'true') {
                        spanString='<span class="line-through">';
                    } else {
                        spanString='<span>';
                    }
                    html += '<li><a href="#_1_' + apiData[j].order + '_' + doc[m].order + '_' + doc[m].desc + '">' + apiData[j].order + '.' + doc[m].order + '.&nbsp;' + spanString + doc[m].desc + '<span></a> </li>';
                }
                html += '</ul>';
                html += '</li>';
            }
        } else {
            for (let i = 0; i < apiGroups.length; i++) {
                let apiGroup = apiGroups[i];
                html += '<li class="'+liClass+'">';
                html += '<a class="dd" href="#_' + apiGroup.name + '">' + apiGroup.order + '.&nbsp;' + apiGroup.name + '</a>';
                html += '<ul class="sectlevel1">';

                let apiData = apiGroup.list;
                for (let j = 0; j < apiData.length; j++) {
                    html += '<li class="'+liClass+'">';
                    html += '<a class="dd" href="#_'+apiGroup.order+'_'+ apiData[j].order + '_'+ apiData[j].link + '">' +apiGroup.order+'.'+ apiData[j].order + '.&nbsp;' + apiData[j].desc + '</a>';
                    html += '<ul class="sectlevel2" style="'+display+'">';
                    doc = apiData[j].list;
                    for (let m = 0; m < doc.length; m++) {
                       let spanString;
                       if (doc[m].deprecated == 'true') {
                           spanString='<span class="line-through">';
                       } else {
                           spanString='<span>';
                       }
                       html += '<li><a href="#_'+apiGroup.order+'_' + apiData[j].order + '_' + doc[m].order + '_' + doc[m].desc + '">'+apiGroup.order+'.' + apiData[j].order + '.' + doc[m].order + '.&nbsp;' + spanString + doc[m].desc + '<span></a> </li>';
                   }
                    html += '</ul>';
                    html += '</li>';
                }

                html += '</ul>';
                html += '</li>';
            }
        }
    }
    return html;
}