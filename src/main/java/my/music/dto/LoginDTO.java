package my.music.dto;

import lombok.Data;

/**
 * 登录参数
 */
@Data
public class LoginDTO {
    /**
     * 账号
     * @mock user123
     */
    private String userAccount;

    /**
     * 密码
     * @mock user123
     */
    private String userPassword;
}
