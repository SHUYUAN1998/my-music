package my.music.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import my.music.pojo.UserInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInfoMapper extends BaseMapper<UserInfo> {
}
