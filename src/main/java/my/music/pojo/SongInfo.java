package my.music.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import my.music.validator.Create;
import my.music.validator.Update;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

/**
 * 歌曲信息
 */
@Data
public class SongInfo extends Model<SongInfo> {
    /**
     * 歌曲id
     */
    @TableId(type = IdType.AUTO)
    @Null(groups = Create.class)
    @NotNull(groups = Update.class)
    private BigInteger id;

    /**
     * 歌曲封面图片文件id
     */
    private BigInteger songImgFileId;

    /**
     * 歌曲名称
     * （最多 30 个字符）
     */
    @NotBlank(groups = Create.class)
    @Length(max = 30)
    private String name;

    /**
     * 歌曲文件id
     */
    @NotNull(groups = Create.class)
    private BigInteger songFileId;

    /**
     * 添加时间
     * @ignore
     */
    @TableField(fill = FieldFill.INSERT)
    private Timestamp gmtCreate;

    /**
     * 修改时间
     * @ignore
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Timestamp gmtModified;

    /**
     * 逻辑删除
     * @ignore
     * @mock false
     */
    @TableField(value = "is_deleted", fill = FieldFill.INSERT)
    @TableLogic
    private Boolean deleted;

}
